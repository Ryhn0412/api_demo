import os
import pytest

from utill.response_util.jira_util import jira_util

if __name__ == '__main__':
    pytest.main(['-vs', '--alluredir', './report/result/'])
    # 生成报告文件
    os.system('allure generate ./report/result/ -o ./report/allure --clean')
    # 登记问题至jira上
    jira_util().add_issue()

