import json
import requests


class request_ryhn:
    '''
    有关request的代码封装
    '''
    session = requests.Session()

    def send_value(self, method, url, data, **kwargs):
        '''
        接口请求方法的核心封装
        :param method: 请求方式
        :param url: 请求路径
        :param data: 请求数据
        :param kwargs: 其它需要的参数
        :return: 请求返回结果
        '''
        method = str(method).lower()
        r = None
        if method == 'get':
            return request_ryhn.session.request(
                method, url=url, params=data, **kwargs)
        else:
            data = json.dumps(data)
            return request_ryhn.session.request(
                method, url=url, data=data, **kwargs)
        # if select == 'json':
        #      # 返回字典格式的数据
        #     return r.json()
        # elif select == 'content':
        #     # 返回字节格式的数据
        #     return r.content
        # elif select == 'headers':
        #     # 返回响应头信息
        #     return r.headers
        # elif select == 'cookies':
        #     # 返回cookie信息
        #     return r.cookies
        # elif select == 'reason':
        #     # 返回状态信息
        #     return r.reason
        # elif select == 'status':
        #     # 返回请求状态码
        #     return r.status_code
        # elif select == 'encoding':
        #     # 返回编码格式
        #     return r.encoding
        # else:
        #     # 返回字符串格式的数据
        #     return r.text