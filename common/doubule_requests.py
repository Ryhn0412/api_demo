import allure

from common.requests import request_ryhn
from utill.create_util.yaml_util import yaml_util


class double_requests(request_ryhn):
    '''
    关于接口请求的二次封装
    '''
    def ryhn_request(self, caseinfo, data):
        '''
        对send_request方法二次封装(读取yaml里的所有参数，并自行添加额外参数）
        :return:
        '''
        with allure.step("接口请求"):
            method = caseinfo['request']['method']
            url = caseinfo['request']['url']
            allure.attach(
                body="{request}".format(request=data),
                name="请求参数",
                attachment_type=allure.attachment_type.TEXT)
            return request_ryhn().send_value(
                method=method, url=url, data=data,headers=yaml_util().read_yaml(
                    yaml_util().yaml_url(r'\yaml_package\header.yaml')
                ), timeout=2.0)

    def ryhn_request_C(self, caseinfo, data):
        '''
        对send_request方法二次封装(读取yaml里的所有参数，并自行添加额外参数）
        :return:
        '''
        with allure.step("接口请求"):
            method = caseinfo['request']['method']
            url = caseinfo['request']['url']
            allure.attach(
                body="{request}".format(request=data),
                name="请求参数",
                attachment_type=allure.attachment_type.TEXT)
            return request_ryhn().send_value(
                method=method, url=url, data=data,headers=yaml_util().read_yaml(
                    yaml_util().yaml_url(r'\yaml_package\header_C.yaml')
                ), timeout=2.0)

    def ryhn_request_M(self, caseinfo, data):
        '''
        对send_request方法二次封装(读取yaml里的所有参数，并自行添加额外参数）
        :return:
        '''
        with allure.step("接口请求"):
            method = caseinfo['request']['method']
            url = caseinfo['request']['url']
            allure.attach(
                body="{request}".format(request=data),
                name="请求参数",
                attachment_type=allure.attachment_type.TEXT)
            return request_ryhn().send_value(
                method=method, url=url, data=data,headers=yaml_util().read_yaml(
                    yaml_util().yaml_url(r'\yaml_package\header_M.yaml')
                ), timeout=2.0)