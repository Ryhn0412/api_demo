import allure
import pytest
from common.doubule_requests import double_requests
from utill.response_util.add_utill import add_util
from utill.response_util.assert_util import assert_ryhn
from utill.request_util.header_util import header_util
from utill.create_util.yaml_util import yaml_util
from utill.response_util.jira_util import jira_util
from utill.response_util.logger_util import MyLog

# -*- author: Ryhn -*-
# -*- encoding: utf-8 -*-
@allure.epic("聚医蕙康后台管理")
@allure.feature("排班管理")
class Test_scheduleInfo_bach_erased:
    '''
    关于排班管理接口类测试
    '''
    @allure.issue("http://10.1.50.165:8080/browse/JYHK20-2989")
    @allure.story("废弃指定排班")
    @allure.title("{caseinfo[name]}")
    @pytest.mark.parametrize("caseinfo", yaml_util().yaml_data(
        url=r'/yaml_package/jyhk/otsp-schedule/schedule_info_controller/scheduleInfo_bach_erased.yaml'))
    @allure.severity(allure.severity_level.CRITICAL)
    @pytest.mark.run(order=-7)
    def test_scheduleInfo_bach_erased(self, caseinfo):
        '''
        互联网医院——废弃指定排班
        :return: 测试结果
        '''
        with allure.step("处理请求数据"):
            # data = caseinfo['request']['data']
            caseinfo2 = yaml_util().yaml_data(
                url=r'/return_message/jyhk/otsp-schedule/schedule_info_controller/schedule-page.yaml'
            )
            # data['id']= caseinfo2['data'][0]['id']
            list1 = ["{id}".format(id=caseinfo2['data'][0]['id'])
                     ]
        with allure.step("发起请求"):
            value = double_requests().ryhn_request(
            caseinfo=caseinfo, data=list1)
        with allure.step("返回请求结果"):
            allure.attach(
                body="{response}".format(response=value.text),
                name="返回数据:",
                attachment_type=allure.attachment_type.TEXT)
        try:
            #   开始断言
            assert_ryhn().exist(except_code=200,
                            really_code=value.status_code)
        except:
            #   断言失败，则调用该语句创建日志（用于上传至jira提交bug）
            MyLog().request_log(caseinfo=caseinfo, value=value, data=list1)