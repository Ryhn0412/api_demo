import allure
import pytest
from common.doubule_requests import double_requests
from utill.response_util.add_utill import add_util
from utill.response_util.assert_util import assert_ryhn
from utill.request_util.header_util import header_util
from utill.create_util.yaml_util import yaml_util
from utill.response_util.jira_util import jira_util


@allure.epic("聚医蕙康通用接口")
@allure.feature("数据字典接口")
class Test_dict:
    '''
    关于数据字典的接口类测试
    '''
    @allure.story("获取数据字典列表")
    @allure.title("{caseinfo[name]}")
    @pytest.mark.parametrize("caseinfo", yaml_util().yaml_data(
        url=r'/yaml_package/jyhk/otsp-system/sys_dict/findList-by_type.yaml'))
    @allure.severity(allure.severity_level.CRITICAL)
    @pytest.mark.run(order=1)
    def test_dict_by_type(self, caseinfo):
        '''
        关于通过数据类型获取数据的接口
        :return: 接口数据
        '''
        # 修改请求头中的contect_type的类型为application/x-www-form-urlencoded
        headers = header_util().change_header(select='ax')
        with allure.step("发起请求"):
            value = double_requests().send_value(
                method=caseinfo['request']['method'],
                headers=headers,
                url=caseinfo['request']['url'],
                data=caseinfo['request']['data'])
        with allure.step("返回请求结果"):
            allure.attach(
                body="{response}".format(response=value.text),
                name="返回数据:",
                attachment_type=allure.attachment_type.TEXT)
        try:
            assert_ryhn().exist(except_code=caseinfo['exceptdata']['except_data']['message'],
                            really_code=value.json())
            add_util().easy_add(value=value.json(),caseinfo=caseinfo)
        except:
            jira_util().create_error(value=value.json(),
                                 caseinfo=caseinfo)