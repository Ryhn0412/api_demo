import allure
import pytest

from common.doubule_requests import double_requests
from common.requests import request_ryhn
from utill.response_util.assert_util import assert_ryhn
from utill.request_util.header_util import header_util
from utill.response_util.json_util import json_util
from utill.create_util.yaml_util import yaml_util


class Test_organization_entity:
    '''
    关于机构实体接口的测试
    '''

    @allure.epic("聚医蕙康后台管理")
    @allure.feature("机构实体管理")
    @allure.story("添加机构实体")
    @allure.title("{caseinfo[name]}")
    @allure.severity(allure.severity_level.CRITICAL)
    @pytest.mark.parametrize("caseinfo", yaml_util().yaml_data(
        url=r'/yaml_package/jyhk/otsp-manage-sys/sys_organization_entity/organization-entity-save.yaml'))
    @pytest.mark.run(order=4)
    def test_organization_entity_save(self, caseinfo):
        '''
        关于添加机构实体（二级机构）接口的测试
        :param caseinfo: 测试数据
        '''
        data = caseinfo['request']['data']
        caseinfo2 = yaml_util().yaml_data(
            url=r'/return_message/jyhk/otsp-manage-sys/sys_organization/descendant-organization.yaml')
        data['oid'] = caseinfo2['id']
        value = double_requests().ryhn_request(
            caseinfo=caseinfo, data=data)
        assert_ryhn().assertEqual(except_code=200, really_code=value.status_code)

    @allure.epic("聚医蕙康后台管理")
    @allure.feature("机构实体资源（M端）")
    @allure.story("获取机构实体列表分页")
    @allure.title("{caseinfo[name]}")
    @allure.severity(allure.severity_level.CRITICAL)
    @pytest.mark.parametrize("caseinfo", yaml_util().yaml_data(
        url=r'/yaml_package/jyhk/otsp-manage-sys/organization_entity/org_entity_page.yaml'))
    @pytest.mark.run(order=4)
    def test_org_entity_page_new(self, caseinfo):
        '''
        关于获取机构下二级机构接口的测试
        :param caseinfo: 测试数据
        '''
        caseinfo2 = yaml_util().yaml_data(
            url=r'/return_message/jyhk/otsp-manage-sys/sys_organization/descendant-organization.yaml')
        data = caseinfo['request']['data']
        data['oid'] = caseinfo2['id']
        value = double_requests().ryhn_request(
            caseinfo=caseinfo, data=data)
        assert_ryhn().exist(
            except_code=caseinfo['exceptdata']['except_data']['message'],
            really_code=value.json())
        with allure.step("存储返回信息中的二级主体id"):
            a = json_util().deepget(except_code="测试", really_code=value.json())
            yaml_util().write_yaml(
                yaml_util().yaml_url(
                    url=r'/return_message/jyhk/otsp-manage-sys/organization_entity/org_entity_page.yaml'),
                data=value.json()['data'][a])

    @allure.epic("聚医蕙康后台管理")
    @allure.feature("机构实体管理")
    @allure.story("删除新建的机构实体")
    @allure.title("{caseinfo[name]}")
    @allure.severity(allure.severity_level.CRITICAL)
    @pytest.mark.parametrize("caseinfo", yaml_util().yaml_data(
        url=r'/yaml_package/jyhk/otsp-manage-sys/sys_organization_entity/organization-entity-erased.yaml'))
    @pytest.mark.run(order=4)
    def test_organization_entity_erased_new(self, caseinfo):
        '''
        关于删除二级机构接口的测试
        :param caseinfo: 测试数据
        '''
        caseinfo2 = yaml_util().yaml_data(
            url=r'/return_message/jyhk/otsp-manage-sys/organization_entity/org_entity_page.yaml')
        url = caseinfo['request']['url'] + "?id=" + str(caseinfo2['id'])
        headers = header_util().change_header(select='ax')
        value = request_ryhn().send_value(
            url=url,
            method=caseinfo['request']['method'],
            data=None,
            headers=headers)
        assert_ryhn().assertEqual(
            except_code=caseinfo['exceptdata']['except_data']['code'],
            really_code=value.status_code)
