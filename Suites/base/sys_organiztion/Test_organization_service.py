import allure
import pytest

from common.doubule_requests import double_requests
from utill.response_util.add_utill import add_util
from utill.response_util.assert_util import assert_ryhn
from utill.response_util.json_util import json_util
from utill.create_util.yaml_util import yaml_util


@allure.epic("聚医蕙康服务管理")
@allure.feature("机构开通服务")
class Test_organization_service:
    '''
    关于机构服务配置接口类的测试
    '''

    @allure.story("添加机构开通服务")
    @allure.title("{caseinfo[name]}")
    @allure.severity(allure.severity_level.CRITICAL)
    @pytest.mark.parametrize("caseinfo", yaml_util().yaml_data(
        url=r'/yaml_package/jyhk/otsp-service/sysorganization_service/organization_service-rel-add.yaml'
    ))
    @pytest.mark.run(order=6)
    def test_organization_service_rel_add(self, caseinfo):
        '''
        关于添加机构开通服务接口的测试
        :param caseinfo: 测试数据
        :return: 返回响应数据
        '''
        caseinfo2 = yaml_util().yaml_data(
            url=r'/return_message/jyhk/otsp-service/servicelnfo/service-service_infos.yaml')
        a = json_util().deepget(except_code='在线复诊',really_code=caseinfo2)
        caseinfo3 = yaml_util().yaml_data(
            url=r'/return_message/jyhk/otsp-manage-sys/sys_organization/descendant-organization.yaml'
        )
        data = caseinfo['request']['data']
        data['serviceId'] = caseinfo2['data'][a]['id']
        data['oid'] = caseinfo3['id']
        value = double_requests().ryhn_request(
            caseinfo=caseinfo,
            data=data)
        assert_ryhn().exist(
            except_code=caseinfo['exceptdata']['except_data']['code'],
            really_code=value.status_code)

    @allure.story("获取机构开通服务列表")
    @allure.title("{caseinfo[name]}")
    @allure.severity(allure.severity_level.CRITICAL)
    @pytest.mark.parametrize("caseinfo", yaml_util().yaml_data(
        url=r'/yaml_package/jyhk/otsp-service/sysorganization_service/organization_service-rels.yaml'))
    @pytest.mark.run(order=6)
    def test_organziation_service_rels(self, caseinfo):
        '''
        关于获取机构开通服务列表接口的测试
        :param caseinfo: 测试数据
        :return: 返回请求结果
        '''
        data = caseinfo['request']['data']
        caseinfo2 = yaml_util().yaml_data(
            url=r'/return_message/jyhk/otsp-manage-sys/sys_organization/descendant-organization.yaml')
        data['oid'] = caseinfo2['id']
        value = double_requests().ryhn_request(
            caseinfo=caseinfo, data=data)
        assert_ryhn().exist(
            except_code=caseinfo['exceptdata']['except_data']['message'],
            really_code=value.json())
        add_util().easy_add(value=value.json(), caseinfo=caseinfo)

    # @allure.story("批量更新机构开通服务")
    # @allure.title("{caseinfo[name]}")
    # @allure.severity(allure.severity_level.CRITICAL)
    # @pytest.mark.parametrize("caseinfo", yaml_util().yaml_data(
    #     url=r'/yaml_package/jyhk/otsp-service/sysorganization_service/organization_service-rel-batch.yaml'))
    # @pytest.mark.run(order=6)
    # def test_organziation_service_rel_batch(self, caseinfo):
    #     '''
    #     关于批量更新机构开通服务接口的测试
    #     :param caseinfo: 测试数据
    #     :return: 返回请求结果
    #     '''
    #     data = caseinfo['request']['data']
    #     caseinfo2 = yaml_util().yaml_data(
    #         url=r'/return_message/jyhk/otsp-manage-sys/sys_organization/descendant-organization.yaml')
    #     for x in range(0, int(length_hint(data))):
    #         data[x]['oid'] = caseinfo2['id']
    #     value = double_requests().ryhn_request(
    #         caseinfo=caseinfo, data=data, select='text')
    #     print(value)
    #     assert_ryhn().assertEqual(except_code=200, really_code=value)
