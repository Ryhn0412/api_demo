import datetime
import random
import re

import allure
import pytest
from common.doubule_requests import double_requests
from common.requests import request_ryhn
from utill.response_util.assert_util import assert_ryhn
from utill.response_util.jira_util import jira_util
from utill.response_util.json_util import json_util
from utill.random_util.random_phone import get_phone_num
from utill.create_util.yaml_util import yaml_util


class Test_sysorganization():
    '''
    关于机构主体的接口
    '''

    @allure.epic("聚医蕙康后台管理")
    @allure.feature("机构主体资源（M端）")
    @allure.story("获取机构列表分页")
    @allure.title("{caseinfo[name]}")
    @allure.severity(allure.severity_level.CRITICAL)
    @pytest.mark.parametrize("caseinfo", yaml_util().yaml_data(
        url=r'/yaml_package/jyhk/otsp-manage-sys/organization_mobile/organization_page.yaml'))
    @pytest.mark.flaky(reruns=1, reruns_flaky=1)
    @pytest.mark.run(order=1)
    def test_organization_page(self, caseinfo):
        '''
        关于获取当前机构主体列表接口的测试
        :param caseinfo: 测试数据
        '''
        data = caseinfo['request']['data']
        value = double_requests().ryhn_request(
            caseinfo=caseinfo, data=data)
        assert_ryhn().exist(
            except_code=caseinfo['exceptdata']['except_data']['message'],
            really_code=value.json())
        yaml_util().write_yaml(url=yaml_util().yaml_url(
            r'/return_message/jyhk/otsp-manage-sys/organization_mobile/organization_page.yaml'), data=value.json()['data'])

    @allure.epic("聚医蕙康后台管理")
    @allure.feature("机构管理")
    @allure.story("添加机构主体信息")
    @allure.title("{caseinfo[name]}")
    @allure.severity(allure.severity_level.CRITICAL)
    @pytest.mark.parametrize("caseinfo", yaml_util().yaml_data(
        url=r'/yaml_package/jyhk/otsp-manage-sys/sys_organization/organization-save.yaml'))
    @pytest.mark.flaky(reruns=1, reruns_flaky=1)
    @pytest.mark.run(order=2)
    def test_organization_save(self, caseinfo):
        '''
        关于添加机构主体接口的测试
        :param caseinfo: 测试数据
        '''
        caseinfo2 = yaml_util().yaml_data(
            url=r'/return_message/jyhk/otsp-manage-sys/organization_mobile/organization_page.yaml')
        data = caseinfo['request']['data']
        data['code'] = "ZD" + get_phone_num() + random.choice(['ajax',
                                                               'bxdf', 'cemg', 'dubg'])
        data['parentId'] = caseinfo2[0]['id']
        value = double_requests().ryhn_request(
            caseinfo=caseinfo, data=data)
        assert_ryhn().assertInclude(
            except_code=caseinfo['exceptdata']['except_data']['message'],
            really_code=value.json())

    @allure.epic("聚医蕙康后台管理")
    @allure.feature("机构管理")
    @allure.story("获取某机构下的子机构")
    @allure.title("{caseinfo[name]}")
    @allure.severity(allure.severity_level.CRITICAL)
    @pytest.mark.parametrize("caseinfo", yaml_util().yaml_data(
        url=r'/yaml_package/jyhk/otsp-manage-sys/sys_organization/descendant-organization.yaml'))
    @pytest.mark.run(order=3)
    def test_descendant_organization(self, caseinfo):
        '''
        关于获取某机构下的子机构接口的测试
        :param caseinfo: 测试数据
        '''
        data = caseinfo['request']['data']
        data['startDate'] = datetime.date.today()
        value = double_requests().ryhn_request(
            caseinfo=caseinfo, data=data)
        assert_ryhn().exist(
            except_code=caseinfo['exceptdata']['except_data']['message'],
            really_code=value.json())
        with allure.step("存储返回信息中的机构主体id"):
            a = json_util().deepget(except_code='周星驰', really_code=value.json())
            yaml_util().write_yaml(
                url=yaml_util().yaml_url(
                    url=r'/return_message/jyhk/otsp-manage-sys/sys_organization/descendant-organization.yaml'),
                data=value.json()['data'][a])

    @allure.epic("聚医蕙康后台管理")
    @allure.feature("机构实体资源（M端）")
    @allure.story("获取机构实体列表分页")
    @allure.title("{caseinfo[name]}")
    @allure.severity(allure.severity_level.CRITICAL)
    @pytest.mark.parametrize("caseinfo", yaml_util().yaml_data(
        url=r'/yaml_package/jyhk/otsp-manage-sys/organization_entity/org_entity_page.yaml'))
    @pytest.mark.run(order=-4)
    def test_org_entity_page(self, caseinfo):
        '''
        关于获取机构下二级机构接口的测试
        :param caseinfo: 测试数据
        '''
        caseinfo2 = yaml_util().yaml_data(
            url=r'/return_message/jyhk/otsp-manage-sys/sys_organization/descendant-organization.yaml')
        data = caseinfo['request']['data']
        data['oid'] = caseinfo2['id']
        value = double_requests().ryhn_request(
            caseinfo=caseinfo, data=data)
        assert_ryhn().exist(
            except_code=caseinfo['exceptdata']['except_data']['message'],
            really_code=value.json())
        with allure.step("存储返回信息中的二级主体id"):
            a = json_util().deepget(except_code="总院", really_code=value.json())
            yaml_util().write_yaml(
                yaml_util().yaml_url(
                    url=r'/return_message/jyhk/otsp-manage-sys/organization_entity/org_entity_page.yaml'),
                data=value.json()['data'][a])

    @allure.epic("聚医蕙康后台管理")
    @allure.feature("机构主体资源")
    @allure.story("修改机构实体")
    @allure.title("{caseinfo[name]}")
    @allure.severity(allure.severity_level.CRITICAL)
    @pytest.mark.parametrize("caseinfo", yaml_util().yaml_data(
        url=r'/yaml_package/jyhk/otsp-manage-sys/sys_organization_entity/organization-entity-modify.yaml'))
    @pytest.mark.run(order=-3)
    def test_organization_entity_modify(self, caseinfo):
        '''
        关于修改二级机构实体信息接口的测试
        :param caseinfo: 测试数据
        '''
        caseinfo2 = yaml_util().yaml_data(
            url=r'/return_message/jyhk/otsp-manage-sys/sys_organization/descendant-organization.yaml')
        caseinfo3 = yaml_util().yaml_data(
            url=r'/return_message/jyhk/otsp-manage-sys/organization_entity/org_entity_page.yaml')
        data = caseinfo['request']['data']
        data['name'] = '自动化医院'
        data['id'] = caseinfo3['id']
        data['oid'] = str(re.search(r"\d+", str(caseinfo2['id'])).group())
        data['code'] = caseinfo3['code']
        data['shortcode'] = caseinfo3['code']
        data['typeId'] = caseinfo3['typeId']
        value = double_requests().ryhn_request(
            caseinfo=caseinfo, data=data)
        try:
            assert_ryhn().assertEqual(except_code=200, really_code=value.status_code)
        except Exception as e:
            jira_util().create_error(caseinfo=caseinfo,value=value.json())


    @allure.epic("聚医蕙康后台管理")
    @allure.feature("机构实体管理")
    @allure.story("删除机构实体")
    @allure.title("{caseinfo[name]}")
    @allure.severity(allure.severity_level.CRITICAL)
    @pytest.mark.parametrize("caseinfo", yaml_util().yaml_data(
        url=r'/yaml_package/jyhk/otsp-manage-sys/sys_organization_entity/organization-entity-erased.yaml'))
    @pytest.mark.run(order=-2)
    def test_organization_entity_erased(self, caseinfo):
        '''
        关于删除二级机构接口的测试
        :param caseinfo: 测试数据
        '''
        caseinfo2 = yaml_util().yaml_data(
            url=r'/return_message/jyhk/otsp-manage-sys/organization_entity/org_entity_page.yaml')
        url = caseinfo['request']['url'] + "?id=" + str(caseinfo2['id'])
        headers = yaml_util().read_yaml(
            yaml_util().yaml_url(r'\yaml_package\header.yaml'))
        headers['Content-Type'] = "application/x-www-form-urlencoded"
        value = request_ryhn().send_value(
            url=url,
            method=caseinfo['request']['method'],
            data=None,
            headers=headers)
        assert_ryhn().assertEqual(
            except_code=caseinfo['exceptdata']['except_data']['code'],
            really_code=value.status_code)

    @allure.epic("聚医蕙康后台管理")
    @allure.feature("机构管理")
    @allure.story("删除机构主体信息")
    @allure.title("{caseinfo[name]}")
    @allure.severity(allure.severity_level.CRITICAL)
    @pytest.mark.parametrize("caseinfo", yaml_util().yaml_data(
        url=r'/yaml_package/jyhk/otsp-manage-sys/sys_organization/organization-erased.yaml'))
    @pytest.mark.run(order=-1)
    def test_organization_erased(self, caseinfo):
        '''
        关于删除机构主体信息接口的测试
        :param caseinfo: 测试数据
        '''
        caseinfo2 = yaml_util().yaml_data(
            url=r'/return_message/jyhk/otsp-manage-sys/sys_organization/descendant-organization.yaml')
        url = caseinfo['request']['url'] + "?id=" + str(caseinfo2['id'])
        headers = yaml_util().read_yaml(
            yaml_util().yaml_url(r'\yaml_package\header.yaml'))
        headers['Content-Type'] = "application/x-www-form-urlencoded"
        value = request_ryhn().send_value(
            url=url,
            method=caseinfo['request']['method'],
            data=None,
            headers=headers)
        assert_ryhn().assertEqual(
            except_code=caseinfo['exceptdata']['except_data']['code'],
            really_code=value.status_code)
