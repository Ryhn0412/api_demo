import allure
import pytest

from common.doubule_requests import double_requests
from utill.response_util.add_utill import add_util
from utill.response_util.assert_util import assert_ryhn
from utill.create_util.yaml_util import yaml_util


@allure.epic("聚医蕙康服务管理")
class Test_department_service:
    '''
    关于科室服务接口类的测试
    '''

    # @allure.feature("机构科室服务接口（M端）")
    # @allure.story("更新机构科室的服务")
    # @allure.title("{caseinfo[name]}")
    # @allure.severity(allure.severity_level.CRITICAL)
    # @pytest.mark.parametrize("caseinfo", yaml_util().yaml_data(
    #     url=r'/yaml_package/jyhk/otsp-service/dept_service_controller/dept-service.yaml'))
    # @pytest.mark.run(order=7)
    # def test_department_service(self,caseinfo):
    #     '''
    #     关于更新所在机构科室的服务
    #     :param caseinfo: 测试数据
    #     :return: 返回请求结果
    #     '''
    #     data = caseinfo['request']['data']
    #     caseinfo2 = yaml_util().yaml_data(
    #         url=r'/return_message/jyhk/otsp-manage-sys/derpartment_controller/org-department-list.yaml')
    #     data[0]['deptId'] = caseinfo2['id']
    #     caseinfo3 = yaml_util().yaml_data(
    #         url=r'/return_message/jyhk/otsp-manage-sys/sys_organization/descendant-organization.yaml')
    #     data[0]['oid'] = caseinfo3['id']
    #     value = double_requests().ryhn_request(
    #         caseinfo=caseinfo, data=data, select='text')
    #     print(value)
    #     assert_ryhn().assertEqual(
    #         except_code=caseinfo['exceptdata']['except_data']['code'],
    #         really_code=value)

    @allure.feature("机构科室服务接口（M端）")
    @allure.story("获取科室服务列表")
    @allure.title("{caseinfo[name]}")
    @allure.severity(allure.severity_level.CRITICAL)
    @pytest.mark.parametrize("caseinfo", yaml_util().yaml_data(
        url=r'/yaml_package/jyhk/otsp-service/dept_service_controller/dept-service-list.yaml'))
    @pytest.mark.run(order=7)
    def test_department_service_list(self, caseinfo):
        '''
        关于获取科室服务列表接口的测试
        :param caseinfo: 测试数据
        :return: 返回请求结果
        '''
        data = caseinfo['request']['data']
        caseinfo2 = yaml_util().yaml_data(
            url=r'/return_message/jyhk/otsp-manage-sys/derpartment_controller/org-department-list.yaml')
        data['deptId'] = caseinfo2['id']
        caseinfo3 = yaml_util().yaml_data(
            url=r'/return_message/jyhk/otsp-manage-sys/sys_organization/descendant-organization.yaml')
        data['oid'] = caseinfo3['id']
        value = double_requests().ryhn_request(
            caseinfo=caseinfo, data=data)
        assert_ryhn().exist(
            except_code=caseinfo['exceptdata']['except_data']['message'],
            really_code=value.json())
        add_util().easy_add(caseinfo=caseinfo, value=value.json())
