import allure
import pytest

from common.doubule_requests import double_requests
from common.requests import request_ryhn
from utill.response_util.assert_util import assert_ryhn
from utill.response_util.json_util import json_util
from utill.create_util.yaml_util import yaml_util


class Test_department:
    '''
    关于医院机构科室的接口测试
    '''

    @allure.epic('聚医蕙康后台管理')
    @allure.feature('后台管理科室管理')
    @allure.story('添加机构科室信息')
    @allure.title("{caseinfo[name]}")
    @allure.severity(allure.severity_level.CRITICAL)
    @pytest.mark.parametrize("caseinfo", yaml_util().yaml_data(
        url=r'/yaml_package/jyhk/otsp-manage-sys/manage_orgdept/orgDept-organization_department_save.yaml'))
    @pytest.mark.run(order=5)
    def test_department_save(self, caseinfo):
        '''
        关于新增医院科室的接口测试
        '''
        data = caseinfo['request']['data']
        caseinfo2 = yaml_util().yaml_data(
            url=r'/return_message/jyhk/otsp-manage-sys/sys_organization/descendant-organization.yaml')
        data['oid'] = caseinfo2['id']
        value = double_requests().ryhn_request(
            caseinfo=caseinfo, data=data)
        assert_ryhn().assertEqual(except_code=200, really_code=value.status_code)

    @allure.epic('聚医蕙康后台管理')
    @allure.feature('科室资源接口（M端）')
    @allure.story('获取机构科室列表')
    @allure.title("{caseinfo[name]}")
    @allure.severity(allure.severity_level.CRITICAL)
    @pytest.mark.parametrize("caseinfo", yaml_util().yaml_data(
        url=r'/yaml_package/jyhk/otsp-manage-sys/derpartment_controller/org-department-list.yaml'))
    @pytest.mark.run(order=5)
    def test_department_list_get1(self, caseinfo):
        '''
        关于获取新增科室信息接口的测试
        :param caseinfo:测试数据
        '''
        caseinfo2 = yaml_util().yaml_data(
            url=r'/return_message/jyhk/otsp-manage-sys/sys_organization/descendant-organization.yaml')
        data = caseinfo['request']['data']
        data['oid'] = caseinfo2['id']
        value = double_requests().ryhn_request(
            caseinfo=caseinfo, data=data)
        assert_ryhn().exist(
            except_code=caseinfo['exceptdata']['except_data']['message'],
            really_code=value.json())
        with allure.step("存储返回信息"):
            a = json_util().deepget(
                except_code=caseinfo['exceptdata']['except_data']['message'],
                really_code=value.json())
            yaml_util().write_yaml(
                yaml_util().yaml_url(
                    url=r'/return_message/jyhk/otsp-manage-sys/derpartment_controller/org-department-list.yaml'),
                data=value.json()[a])
    #
    # @allure.epic('聚医惠康后台管理')
    # @allure.feature('后台管理科室管理')
    # @allure.story('获取机构科室列表')
    # @allure.title("{caseinfo[name]}")
    # @allure.severity(allure.severity_level.CRITICAL)
    # @pytest.mark.parametrize("caseinfo", yaml_util().yaml_data(
    #     url=r'/yaml_package/jyhk/otsp-manage-sys/manage_orgdept/orgDept-organization_departments_page_get.yaml'))
    # @pytest.mark.run(order=5)
    # def test_department_list_get2(self, caseinfo):
    #     '''
    #     关于获取机构科室列表接口的测试
    #     :param caseinfo: 测试数据
    #     '''
    #     caseinfo2 = yaml_util().yaml_data(
    #         url=r'/return_message/jyhk/otsp-manage-sys/sys_organization/descendant-organization.yaml')
    #     data = caseinfo['request']['data']
    #     data['oid'] = caseinfo2['id']
    #     value = double_requests().ryhn_request(
    #         caseinfo=caseinfo, data=data, select='json')
    #     print(data)
    #     assert_ryhn().exist(
    #         except_code=caseinfo['exceptdata']['except_data'],
    #         really_code=value)
    #
    # @allure.epic('聚医惠康后台管理')
    # @allure.feature('后台管理科室管理')
    # @allure.story('获取机构科室列表无分页')
    # @allure.title("{caseinfo[name]}")
    # @allure.severity(allure.severity_level.CRITICAL)
    # @pytest.mark.parametrize("caseinfo", yaml_util().yaml_data(
    #     url=r'/yaml_package/jyhk/otsp-manage-sys/manage_orgdept/orgDept-departments_get.yaml'))
    # @pytest.mark.run(order=5)
    # def test_department_list_get3(self, caseinfo):
    #     '''
    #     关于获取机构科室列表接口的测试
    #     :param caseinfo: 测试数据
    #     '''
    #     caseinfo2 = yaml_util().yaml_data(
    #         url=r'/return_message/jyhk/otsp-manage-sys/sys_organization/descendant-organization.yaml')
    #     data = caseinfo['request']['data']
    #     data['oid'] = caseinfo2['id']
    #     data['name'] = caseinfo2['name']
    #     value = double_requests().ryhn_request(
    #         caseinfo=caseinfo, data=data, select='json')
    #     print(data)
    #     assert_ryhn().exist(
    #         except_code=caseinfo['exceptdata']['except_data']['message'],
    #         really_code=value)

    @allure.epic('聚医蕙康后台管理')
    @allure.feature('后台管理科室管理')
    @allure.story('修改机构科室信息')
    @allure.title("{caseinfo[name]}")
    @allure.severity(allure.severity_level.CRITICAL)
    @pytest.mark.parametrize("caseinfo", yaml_util().yaml_data(
        url=r'/yaml_package/jyhk/otsp-manage-sys/manage_orgdept/orgDept-organization_department_modify.yaml'))
    @pytest.mark.run(order=5)
    def test_department_modify(self, caseinfo):
        '''
        关于修改机构科室信息接口的测试
        :param caseinfo: 测试数据
        '''
        data = caseinfo['request']['data']
        caseinfo2 = yaml_util().yaml_data(
            url=r'/return_message/jyhk/otsp-manage-sys/sys_organization/descendant-organization.yaml')
        caseinfo3 = yaml_util().yaml_data(
            url=r'/return_message/jyhk/otsp-manage-sys/derpartment_controller/org-department-list.yaml')
        data['oid'] = caseinfo2['id']
        data['id'] = caseinfo3['id']
        value = double_requests().ryhn_request(
            caseinfo=caseinfo, data=data)
        assert_ryhn().exist(
            except_code=caseinfo['exceptdata']['except_data']['code'],
            really_code=value.status_code)

    @allure.epic('聚医蕙康后台管理')
    @allure.feature('后台管理科室管理')
    @allure.story('删除机构科室信息')
    @allure.title("{caseinfo[name]}")
    @allure.severity(allure.severity_level.CRITICAL)
    @pytest.mark.parametrize("caseinfo", yaml_util().yaml_data(
        url=r'/yaml_package/jyhk/otsp-manage-sys/manage_orgdept/orgDept_organization_department_earsed.yaml'))
    @pytest.mark.run(order=-5)
    def test_department_delete(self, caseinfo):
        '''
        关于删除机构科室信息接口的测试
        :param caseinfo: 测试数据
        '''
        with allure.step("改变请求头的Content-Type类型"):
            caseinfo2 = yaml_util().yaml_data(
                url=r'/return_message/jyhk/otsp-manage-sys/derpartment_controller/org-department-list.yaml')
            url = caseinfo['request']['url'] + "?id=" + str(caseinfo2['id'])
            headers = yaml_util().read_yaml(
                yaml_util().yaml_url(r'\yaml_package\header.yaml'))
            headers['Content-Type'] = "application/x-www-form-urlencoded"
        with allure.step("发起请求"):
            value = request_ryhn().send_value(headers=headers, url=url, data=None,
                                             method=caseinfo['request']['method'])
        assert_ryhn().assertEqual(except_code=200, really_code=value.status_code)
