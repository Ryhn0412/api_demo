import allure
import pytest

from common.doubule_requests import double_requests
from utill.response_util.add_utill import add_util
from utill.response_util.assert_util import assert_ryhn
from utill.create_util.yaml_util import yaml_util


class Test_doctor_sign:
    '''
    关于医生签约接口类的测试
    '''

    @allure.epic("聚医蕙康后台管理")
    @allure.feature("医生签约接口（M端）")
    @allure.story("签约医生")
    @allure.title("{caseinfo[name]}")
    @allure.severity(allure.severity_level.CRITICAL)
    @pytest.mark.parametrize("caseinfo", yaml_util().yaml_data(
        url=r'/yaml_package/jyhk/otsp-manage-sys/doctor_sign_controller/doctor-sign.yaml'))
    @pytest.mark.run(order=7)
    def test_doctor_sign(self, caseinfo):
        '''
        关于医生签约接口的测试
        :param casinfo: 测试数据
        :return: 返回请求结果
        '''
        data = caseinfo['request']['data']
        # 获取请求数据的机构id
        caseinfo2 = yaml_util().yaml_data(
            url=r'/return_message/jyhk/otsp-manage-sys/organization_entity/org_entity_page.yaml')
        data['oid'] = caseinfo2['id']
        # 获取请求数据的医生id
        caseinfo3 = yaml_util().yaml_data(
            url=r'/return_message/jyhk/otsp-manage-sys/doctor_controller/manage-doctor-page.yaml')
        data['uid'] = caseinfo3[0]['id']
        # 获取请求数据的科室Id
        caseinfo4 = yaml_util().yaml_data(
            url=r'/return_message/jyhk/otsp-manage-sys/derpartment_controller/org-department-list.yaml')
        data['deptId'] = caseinfo4['id']
        value = double_requests().ryhn_request(
            caseinfo=caseinfo, data=data)
        assert_ryhn().assertEqual(
            except_code=caseinfo['exceptdata']['except_data']['code'],
            really_code=value.status_code)

    @allure.epic("聚医蕙康后台管理")
    @allure.feature("医生签约接口（M端）")
    @allure.story("获取医生签约信息")
    @allure.title("{caseinfo[name]}")
    @allure.severity(allure.severity_level.CRITICAL)
    @pytest.mark.parametrize("caseinfo", yaml_util().yaml_data(
        url=r'/yaml_package/jyhk/otsp-manage-sys/doctor_sign_controller/doctor-sign-page.yaml'))
    @pytest.mark.run(order=7)
    def test_doctor_sign_page(self, caseinfo):
        '''
        关于获取医生签约信息接口的测试
        :param casinfo: 测试数据
        :return: 返回请求结果
        '''
        data = caseinfo['request']['data']
        # 获取请求数据中的oid
        caseinfo2 = yaml_util().yaml_data(
            url=r'/return_message/jyhk/otsp-manage-sys/organization_entity/org_entity_page.yaml')
        data['oid'] = caseinfo2['id']
        value = double_requests().ryhn_request(
            caseinfo=caseinfo, data=data)
        assert_ryhn().exist(
            except_code=caseinfo['exceptdata']['except_data']['message'],
            really_code=value.json())
        add_util().easy_add(value=value.json(), caseinfo=caseinfo)

    @allure.epic("聚医蕙康后台管理")
    @allure.feature("医生签约接口（M端）")
    @allure.story("修改医生签约信息")
    @allure.title("{caseinfo[name]}")
    @allure.severity(allure.severity_level.CRITICAL)
    @pytest.mark.parametrize("caseinfo", yaml_util().yaml_data(
        url=r'/yaml_package/jyhk/otsp-manage-sys/doctor_sign_controller/modify-doctor-sign_info.yaml'))
    @pytest.mark.run(order=7)
    def test_doctor_sign_modify(self, caseinfo):
        '''
        关于修改医生签约信息接口的测试
        :param caseinfo:测试数据
        :return:返回请求数据
        '''
        data = caseinfo['request']['data']
        caseinfo4 = yaml_util().yaml_data(
            url=r'/return_message/jyhk/otsp-manage-sys/derpartment_controller/org-department-list.yaml')
        data['deptId'] = caseinfo4['id']
        caseinfo2 = yaml_util().yaml_data(
            url=r'/return_message/jyhk/otsp-manage-sys/doctor_sign_controller/doctor-sign-page.yaml')
        data['id'] = caseinfo2['data'][0]['id']
        data['remark'] = "这个医生不太冷"
        value = double_requests().ryhn_request(
            caseinfo=caseinfo, data=data)
        assert_ryhn().exist(
            except_code=caseinfo['exceptdata']['except_data']['code'],
            really_code=value.status_code)

    @allure.epic("聚医蕙康后台管理")
    @allure.feature("医生签约接口（M端）")
    @allure.story("与医生解除合约")
    @allure.title("{caseinfo[name]}")
    @allure.severity(allure.severity_level.CRITICAL)
    @pytest.mark.parametrize("caseinfo", yaml_util().yaml_data(
        url=r'\yaml_package/jyhk\otsp-manage-sys\doctor_sign_controller\eteled-doctor-sign.yaml'))
    @pytest.mark.run(order=-5)
    def test__doctor_sign_eteled(self, caseinfo):
        '''
        关于医生解除合约接口的测试
        :param caseinfo: 测试数据
        :return: 返回请求结果
        '''
        data = caseinfo['request']['data']
        caseinfo2 = yaml_util().yaml_data(
            url=r'\return_message/jyhk\otsp-manage-sys\doctor_sign_controller\doctor-sign-page.yaml')
        data['id'] = caseinfo2['data'][0]['id']
        value = double_requests().ryhn_request(
            caseinfo=caseinfo, data=data)
        assert_ryhn().assertEqual(except_code=200, really_code=value.status_code)
