import allure
import pytest
from common.doubule_requests import double_requests
from utill.response_util.add_utill import add_util
from utill.response_util.assert_util import assert_ryhn
from utill.create_util.yaml_util import yaml_util


class Test_doctor_service:
    '''
    关于医生服务列表接口类的测试
    '''
    @allure.epic("聚医蕙康服务管理")
    @allure.feature("医生服务接口（M端）")
    @allure.story("更新医生所在机构的服务")
    @allure.title("{caseinfo[name]}")
    @allure.severity(allure.severity_level.CRITICAL)
    @pytest.mark.parametrize("caseinfo", yaml_util().yaml_data(
        url=r'/yaml_package/jyhk/otsp-service/doctor_service_controller/doctor-service.yaml'))
    @pytest.mark.run(order=8)
    def test_doctor_service(self, caseinfo):
        '''
        关于更新医生所在机构科室的服务
        :param caseinfo: 测试数据
        :return: 返回请求结果
        '''
        data = caseinfo['request']['data']
        caseinfo2 = yaml_util().yaml_data(
            url=r'/return_message/jyhk/otsp-manage-sys/sys_organization/descendant-organization.yaml')
        data[0]['oid'] = caseinfo2['id']
        caseinfo3 = yaml_util().yaml_data(
            url=r'/return_message/jyhk/otsp-manage-sys/doctor_controller/manage-doctor-page.yaml')
        data[0]['uid'] = caseinfo3[0]['id']
        value = double_requests().ryhn_request(
            caseinfo=caseinfo, data=data)

        assert_ryhn().assertEqual(
            except_code=caseinfo['exceptdata']['except_data']['code'],
            really_code=value.status_code)

    @allure.epic("聚医蕙康服务管理")
    @allure.feature("医生服务接口（M端）")
    @allure.story("获取医生服务列表")
    @allure.title("{caseinfo[name]}")
    @pytest.mark.parametrize("caseinfo", yaml_util().yaml_data(
        url=r'/yaml_package/jyhk/otsp-service/doctor_service_controller/doctor-service-list.yaml'))
    @pytest.mark.run(order=8)
    def test_doctor_service_list(self, caseinfo):
        '''
        关于获取医生服务列表接口的测试
        :param caseinfo: 测试数据
        :return: 返回请求结果
        '''
        data = caseinfo['request']['data']
        caseinfo2 = yaml_util().yaml_data(
            url=r'/return_message/jyhk/otsp-manage-sys/sys_organization/descendant-organization.yaml')
        data['oid'] = caseinfo2['id']
        caseinfo3 = yaml_util().yaml_data(
            url=r'/return_message/jyhk/otsp-manage-sys/doctor_controller/manage-doctor-page.yaml')
        data['uid'] = caseinfo3[0]['id']
        value = double_requests().ryhn_request(
            caseinfo=caseinfo, data=data)
        assert_ryhn().exist(
            except_code=caseinfo['exceptdata']['except_data']['message'],
            really_code=value.json())
        add_util().easy_add(value=value.json(), caseinfo=caseinfo)
