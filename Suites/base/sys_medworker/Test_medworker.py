import allure
import pytest

from common.doubule_requests import double_requests
from utill.random_util import random_idcard
from utill.response_util.add_utill import add_util
from utill.response_util.assert_util import assert_ryhn
from utill.response_util.jira_util import jira_util
from utill.response_util.json_util import json_util
from utill.create_util.yaml_util import yaml_util


class Test_medworker:
    '''
    关于医务工作者管理接口集的测试
    '''

    @allure.epic("聚医蕙康后台管理")
    @allure.feature("医务工作者管理")
    @allure.story("新增医务工作者")
    @allure.title("{caseinfo[name]}")
    @allure.severity(allure.severity_level.CRITICAL)
    @pytest.mark.parametrize("caseinfo", yaml_util().yaml_data(
        url=r'/yaml_package/jyhk/otsp-manage-sys/sys_medworker/med_worker_add.yaml'))
    @pytest.mark.run(order=6)
    @pytest.mark.flaky(reruns=2, reruns_delay=2)
    def test_medworker_add(self, caseinfo):
        '''
        关于新增医务工作者接口的测试
        :param caseinfo: 测试数据
        :return: 接口返回信息
        '''
        caseinfo2 = yaml_util().yaml_data(
            url=r'/return_message/jyhk/otsp-system/sys_dict/findList-by_type_doctor_first.yaml')
        a = json_util().deepget(except_code='医疗', really_code=caseinfo2)
        caseinfo3 = yaml_util().yaml_data(
            url=r'/return_message/jyhk/otsp-system/sys_dict/findList-by_type_doctor_second.yaml')
        b = json_util().deepget(except_code='主任医师', really_code=caseinfo3)
        caseinfo4 = yaml_util().yaml_data(
            url=r'/return_message/jyhk/otsp-manage-sys/derpartment_controller/org-department-list.yaml')
        caseinfo5 = yaml_util().yaml_data(
            url=r'/return_message/jyhk/otsp-manage-sys/organization_entity/org_entity_page.yaml')
        with allure.step("获取请求数据"):
            data = caseinfo['request']['data']
            data['userParam']['cardNum'] = str(random_idcard.id_card())
            data['medWorkerInfoParam']['positionalFirstId'] = caseinfo2[a]['id']
            data['medWorkerInfoParam']['positionalSecondId'] = caseinfo3[b]['id']
            data['speciality'] = '我会编程'
            data['medWorkerInfoParam']['deptId'] = caseinfo4['id']
            data['oid'] = str(caseinfo5['id'])
        value = double_requests().ryhn_request(
            caseinfo=caseinfo, data=data)
        jira_util().easy_add(value=value.text,
                             caseinfo=caseinfo)
        assert_ryhn().exist(
            except_code=caseinfo['exceptdata']['except_data']['code'],
            really_code=value.status_code)


    @allure.epic("聚医蕙康后台管理")
    @allure.feature("医生资源接口（M端）")
    @allure.story("获取医生列表分页")
    @allure.title("{caseinfo[name]}")
    @allure.severity(allure.severity_level.CRITICAL)
    @pytest.mark.parametrize("caseinfo", yaml_util().yaml_data(
        url=r'/yaml_package/jyhk/otsp-manage-sys/doctor_controller/manage-doctor-page.yaml'))
    @pytest.mark.run(order=6)
    def test_medworker_get(self, caseinfo):
        '''
        关于获取医生列表分页接口的测试
        :param caseinfo: 测试数据
        :return: 返回请求结果
        '''
        data = caseinfo['request']['data']
        caseinfo2 = yaml_util().yaml_data(
            url=r'/return_message/jyhk/otsp-manage-sys/organization_entity/org_entity_page.yaml')
        data['oid'] = caseinfo2['id']
        value = double_requests().ryhn_request(
            caseinfo=caseinfo, data=data)
        assert_ryhn().exist(
            except_code=caseinfo['exceptdata']['except_data']['message'],
            really_code=value.json()['data'])
        add_util().easy_add(value=value.json()['data'], caseinfo=caseinfo)

    @allure.epic("聚医蕙康后台管理")
    @allure.feature("医务工作者管理")
    @allure.story("删除医生与机构关系")
    @allure.title("{caseinfo[name]}")
    @allure.severity(allure.severity_level.CRITICAL)
    @pytest.mark.parametrize("caseinfo", yaml_util().yaml_data(
        url=r'/yaml_package/jyhk/otsp-manage-sys/sys_medworker/med_worker_eteled.yaml'))
    @pytest.mark.run(order=6)
    def test_medworker_eteled(self, caseinfo):
        '''
        关于删除医生与机构关系接口的测试（机构管理）
        :param caseinfo: 测试数据
        :return: 返回请求信息
        '''
        data = caseinfo['request']['data']
        caseinfo2 = yaml_util().yaml_data(
            url=r'/return_message/jyhk/otsp-manage-sys/doctor_controller/manage-doctor-page.yaml')
        caseinfo3 = yaml_util().yaml_data(
            url=r'/return_message/jyhk/otsp-manage-sys/organization_entity/org_entity_page.yaml')
        data['oid'] = caseinfo3['id']
        data['uid'] = caseinfo2[0]['id']
        value = double_requests().ryhn_request(
            caseinfo=caseinfo, data=data)
        assert_ryhn().assertEqual(
            except_code=caseinfo['exceptdata']['except_data']['code'],
            really_code=value.status_code)
