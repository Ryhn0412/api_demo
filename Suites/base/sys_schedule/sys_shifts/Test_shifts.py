import allure
import pytest

from common.doubule_requests import double_requests
from utill.response_util.add_utill import add_util
from utill.response_util.assert_util import assert_ryhn
from utill.response_util.jira_util import jira_util
from utill.response_util.json_util import json_util
from utill.create_util.yaml_util import yaml_util


@allure.epic("聚医蕙康排班管理")
@allure.feature("班次管理")
class Test_Shifts:
    '''
    关于班次部分接口类的测试
    '''
    @allure.story("新增班次")
    @allure.title("{caseinfo[name]}")
    @allure.severity(allure.severity_level.CRITICAL)
    @pytest.mark.parametrize("caseinfo", yaml_util().yaml_data(
        url=r'/yaml_package/jyhk/otsp-schedule/shifts/schedule-shifts_add.yaml'))
    @pytest.mark.run(order=9)
    def test_shifts_add(self, caseinfo):
        '''
        关于添加班次接口的测试
        :param caseinfo: 测试数据
        :return: 返回请求结果
        '''
        data = caseinfo['request']['data']
        caseinfo2 = yaml_util().yaml_data(
            url=r'/return_message/jyhk/otsp-system/sys_dict/findList-by_type_shifts.yaml')
        caseinfo3 = yaml_util().yaml_data(
            url=r'/return_message/jyhk/otsp-manage-sys/sys_organization/descendant-organization.yaml')
        a = json_util().deepget(except_code="上午", really_code=caseinfo2)
        data['code'] = caseinfo2[a]['dictCode']
        data['name'] = caseinfo2[a]['dictLabel']
        data['oid'] = caseinfo3['id']
        data['startTime'] = '03:00'
        data['endTime'] = '23:59'
        data['registerStartTime'] = '03:00'
        data['registerEndTime'] = '23:59'
        value = double_requests().ryhn_request(
            caseinfo=caseinfo,
            data=data
            )
        try:
            assert_ryhn().exist(
                except_code=caseinfo['exceptdata']['except_data']['code'],
                really_code=value.status_code)
        except:
            jira_util().create_error(caseinfo=caseinfo, value='{a}'.format(a=value.json()))

    @allure.story("获取班次")
    @allure.title("{caseinfo[name]}")
    @allure.severity(allure.severity_level.CRITICAL)
    @pytest.mark.parametrize("caseinfo", yaml_util().yaml_data(
        url=r'/yaml_package/jyhk/otsp-schedule/shifts/schedule-shifts_get.yaml'))
    @pytest.mark.run(order=9)
    def test_shifts_get(self, caseinfo):
        '''
        关于获取班次接口的测试
        :param caseinfo:测试数据
        :return:返回请求结果
        '''
        data = caseinfo['request']['data']
        caseinfo3 = yaml_util().yaml_data(
            url=r'/return_message/jyhk/otsp-manage-sys/sys_organization/descendant-organization.yaml')
        data['oid'] = caseinfo3['id']
        value = double_requests().ryhn_request(
            data=data, caseinfo=caseinfo)
        assert_ryhn().exist(
            except_code=caseinfo['exceptdata']['except_data']['message'],
            really_code=value.json())
        add_util().easy_add(value=value.json(), caseinfo=caseinfo)

    @allure.story("删除班次")
    @allure.title("{caseinfo[name]}")
    @allure.severity(allure.severity_level.CRITICAL)
    @pytest.mark.parametrize("caseinfo", yaml_util().yaml_data(
        url=r'/yaml_package/jyhk/otsp-schedule/shifts/schedule-shifts_erased.yaml'))
    @pytest.mark.run(order=-6)
    def test_shifts_erased(self, caseinfo):
        '''
        关于删除班次接口的测试
        :param caseinfo: 测试数据
        :return: 返回请求数据
        '''
        data = caseinfo['request']['data']
        caseinfo2 = yaml_util().yaml_data(
            url=r'/return_message/jyhk/otsp-schedule/shifts/schedule-shifts_get.yaml')
        data['id'] = caseinfo2['data'][1]['id']
        url = caseinfo['request']['url'] + '?id=' + data['id']
        value = double_requests().send_value(
            method="POST", url=url, data=None)
        assert_ryhn().exist(
            except_code=caseinfo['exceptdata']['except_data']['code'],
            really_code=value.status_code)
