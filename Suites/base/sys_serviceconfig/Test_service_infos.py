from utill.response_util.logger_util import MyLog
import allure
import pytest

from common.doubule_requests import double_requests
from utill.response_util.add_utill import add_util
from utill.response_util.assert_util import assert_ryhn
from utill.create_util.yaml_util import yaml_util


@allure.epic("聚医蕙康后台管理")
@allure.feature("服务信息")
class Test_service_infos:
    '''
    关于服务信息列表接口类的测试
    '''

    @allure.story("获取服务信息列表")
    @allure.title("{caseinfo[name]}")
    @allure.severity(allure.severity_level.CRITICAL)
    @pytest.mark.parametrize("caseinfo", yaml_util().yaml_data(
        url=r'/yaml_package/jyhk/otsp-service/servicelnfo/service-service_infos.yaml'
    ))
    @pytest.mark.run(order=6)
    def test_service_infos(self, caseinfo):
        '''
        关于获取服务信息列表接口的测试
        :param caseinfo: 测试数据
        :return: 返回请求结果
        '''
        value = double_requests().ryhn_request(
            caseinfo=caseinfo, data=None)
        try:
            assert_ryhn().exist(
                really_code=value.json(),
                except_code=caseinfo['exceptdata']['except_data']['message'])
            add_util().easy_add(value=value.json(), caseinfo=caseinfo)
        except:
            MyLog().request_log(caseinfo=caseinfo,data=None,value=value)
