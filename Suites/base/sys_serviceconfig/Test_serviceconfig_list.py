from utill.response_util.logger_util import MyLog
import allure
import pytest

from common.doubule_requests import double_requests
from utill.response_util.add_utill import add_util
from utill.response_util.assert_util import assert_ryhn
from utill.create_util.yaml_util import yaml_util


@allure.epic("聚医蕙康服务管理")
@allure.feature("平台与机构服务配置信息")
class Test_serviceconfig:
    '''
    关于服务配置类接口的测试
    '''

    @allure.story("获取平台与机构服务配置信息列表")
    @allure.title("{caseinfo[name]}")
    @allure.severity(allure.severity_level.CRITICAL)
    @pytest.mark.parametrize("caseinfo", yaml_util().yaml_data(
        url=r'/yaml_package/jyhk/otsp-service/serviceconfig/service_config_list.yaml'))
    def test_service_config_list(self, caseinfo):
        '''
        关于获取平台与机构服务配置信息列表接口的测试
        :param caseinfo: 测试数据
        :return: 返回请求结果
        '''
        data = caseinfo['request']['data']
        value = double_requests().ryhn_request(
            caseinfo=caseinfo, data=data)
        try:
            assert_ryhn().exist(
                except_code=caseinfo['exceptdata']['except_data']['message'],
                really_code=value.json())
            add_util().easy_add(value=value.json(), caseinfo=caseinfo)
        except:
            MyLog().request_log(caseinfo=caseinfo,data=data,value=value)
