import allure
from deepdiff import grep


class assert_ryhn:
    '''
    有关断言封装的工具类
    '''
    def assertEqual(self, except_code, really_code):
        '''
        断言预期值是否等于实际值
        :param except_code:预期值
        :param really_code: 实际值
        :return: 返回断言结果
        '''
        with allure.step("开始断言"):
            assert except_code == really_code

    def assertnot(self, except_code, really_code):
        '''
        断言预期值是否不等于实际值
        :param except_code:预期值
        :param really_code: 实际值
        :return: 返回断言结果
        '''
        with allure.step("开始断言"):
            assert really_code != except_code

    def assertInclude(self,except_code,really_code):
        '''
        断言预期内容是否存在于实际内容中
        :param except_code: 预期内容
        :param really_code: 实际内容
        :return: 返回断言结果
        '''
        with allure.step("开始断言"):
            assert except_code in really_code

    def exist(self,except_code,really_code):
        '''
        断言预期内容是否存在于实际值当中
        :return:
        '''
        with allure.step("开始断言"):
            a = really_code | grep(except_code)
            assert a != {}