import allure

from utill.create_util.yaml_util import yaml_util


class add_util(yaml_util):
    '''
    关于存储数据的工具类
    '''
    def easy_add(self, value, caseinfo):
        '''
        封装后的存储返回信息的语句（通过yaml工具类的三次封装）
        :return:
        '''
        with allure.step("存储返回信息"):
            yaml_util().write_yaml(
                yaml_util().yaml_url(
                    url=str(r"/return_message")+str(caseinfo['exceptdata']['return_url'])),
                data=value)