import allure

from utill.create_util.yaml_util import yaml_util


class header_util(yaml_util):
    '''
    关于请求头更改的工具类
    '''
    def change_header(self,select):
        '''
        更改请求头的contect-Type类型
        :return:返回你想要的请求头
        '''
        with allure.step("改变请求头的Content-Type类型"):
            headers = yaml_util().read_yaml(
                yaml_util().yaml_url(r'\yaml_package\header.yaml'))
            if select =='ax':
                headers['Content-Type'] = "application/x-www-form-urlencoded"
                return headers
            elif select =='mf':
                headers['Content-Type'] = "multipart/form-data"
                return headers
            elif select =='aj':
                headers['Content-Type'] = "application/json"
                return headers
            elif select =='tx':
                headers['Content-Type'] = "text/xml"
                return headers



